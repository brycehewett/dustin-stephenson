<div class="column-one-third-last sidebar">

    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('right-sidebar')) : else : ?>
    
        <!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->

    	<?php get_search_form(); ?>
        
    	<h3>Archives</h3>
    	<ul>
    		<?php wp_get_archives('type=monthly'); ?>
    	</ul>
        
        <!--<h3>Categories</h3>
        <ul>
    	   <?php wp_list_categories('show_count=1&title_li='); ?>
        </ul>-->
        
	<?php endif; ?>

</div><!--/.column-one-third-last sidebar-->