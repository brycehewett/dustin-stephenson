<header class="top-nav">
  <div class="container">
    <div class="logo-wrapper">
      <a class="logo" href="<?php echo get_option('home'); ?>/" alt="<?php bloginfo('name'); ?>">
        <img src="<?php echo get_template_directory_uri() . '/images/logo.svg';?>"/>
      </a>
      <span class="logo-contact"><a href="tel:850-215-5150">850.215.5150</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="mailto:dustin@dustinstephenson.com">dustin@dustinstephenson.com</a></span>
    </div>
    <div class="menu-toggle"><i class="fa fa-bars"></i></div>
    <?php $defaults = array(
      'menu'            => 'header-menu', 
      'container'       => '',
      'echo'            => true,
      'fallback_cb'     => 'wp_page_menu',
      'items_wrap'      => '<nav class="header-menu"><ul>%3$s</ul></nav>',
      'depth'           => 2,);
    wp_nav_menu( $defaults ); ?>
  </div>
</header>
