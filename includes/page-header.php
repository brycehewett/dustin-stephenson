<?php
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$url = $thumb['0'];
?>
<?php if ( has_post_thumbnail() ) { ?>

	<div class="header-image" style="background-image:url(<?=$url?>);"></div>

<?php } ?>