<article class="post-teaser">
  
  <header>
    <?php if ( has_post_thumbnail() ) { ?>
      <a class="teaser-image" href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php the_post_thumbnail('large'); ?></a>
    <?php } ?>
    
    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
    <?php include (TEMPLATEPATH . '/includes/meta.php' ); ?>
  </header>

  <?php wpe_excerpt('wpe_excerptlength_index', 'wpe_excerptmore'); ?>

</article>