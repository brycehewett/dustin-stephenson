<?php get_header(); ?>

<div class="two-column-container">

	<div class="column-two-thirds">
		<?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

		<header>
    	<h1><?php the_title(); ?></h1>
    	<?php include (TEMPLATEPATH . '/includes/meta.php' );?>
  	</header>

  	<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); }
			
		the_content(); ?>

		<footer>
			<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
			<?php the_tags( 'Tags: ', ', ', ''); ?>
						
			<div id="post-author">
				<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_email(), '58' ); /* This avatar is the user's gravatar (http://gravatar.com) based on their administrative email address */  } ?>
				
				<?php $shortlink = wp_get_shortlink(); ?>

				<div id="author-description">
					<p>Written by <?php the_author_posts_link() ?><br />
					<a class="share_twitter" title="Tweet This" target="_blank" href="http://twitter.com/home?status=<?php echo str_replace("|", "", get_the_title()); echo(' '); echo $shortlink; ?>"><i class="fa fa-twitter-square"></i> Tweet This</a>

					<a class="share_facebook" title="Share This" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" target="_blank"><i class="fa fa-facebook-square"></i> Share This</a></p>

				</div>
			</div><!--#post-author-->
		</footer>
	</article>
		
	<div id="post-nav">
		<div class="older">
			<?php previous_post_link('%link', '&laquo; Previous Post') ?>
		</div><!--.older-->
		<div class="newer">
			<?php next_post_link('%link', 'Next Post &raquo;') ?>
		</div><!--.older-->
	</div>

	<section id="comments">
		<?php comments_template( '', true ); ?>
	</section>
	
	<?php endwhile; endif; ?>
	</div><!--/.column-two-thirds-->

	<?php get_sidebar()?>

</div><!--/.two-column-container-->


	
<?php get_footer(); ?>