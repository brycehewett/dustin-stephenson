

      <footer class="footer" class="source-org vcard copyright">
        <div class="container">
        <div class="one-quarter-col">
          <a href="#" class="footer-logo"></a>
        </div>
        <div class="three-quarter-col">
          <?php $defaults = array(
          'menu'            => 'footer-menu', 
          'container'       => '',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'items_wrap'      => '<nav class="footer-menu"><ul>%3$s</ul></nav>',
          'depth'           => 1,);
          wp_nav_menu( $defaults ); ?>

          <?php dynamic_sidebar('footer-content')?>
          
          <p class="copyright">1156 Jenks Avenue  •  Panama City, FL 32401  •  <a href="mailto:dustin@dustinstephenson.com">dustin@dustinstephenson.com</a>  •  Fax: 850.215.8845  •  Phone: <a href="tel:850-215-5150">850.215.5150</a><br/>
          &copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?> All rights reserved  •  <a href="/privacy-policy">Privacy Policy</a>
          </p>
        </div>
      </div>
      </footer>
       
        <?php wp_footer(); ?>

        <?php  
          if ( function_exists( 'ot_get_option' ) ) {
            $google_analytics = ot_get_option( 'google_analytics' );
          
          if ( ! empty( $google_analytics ) ) {


        echo "<script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', '".$google_analytics."', 'auto');
                ga('send', 'pageview');

              </script>";
          
            }
             
        }?>
      </div><!--.content-->
    </div><!--.site-wrapper-->   
  </body>
</html>
