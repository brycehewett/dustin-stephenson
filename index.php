<?php get_header(); ?>
<div class="promo-header">
    <div class="container">
        <div class="promo-img"></div>
        <div class="promo-text">
            <img src="<?php print get_template_directory_uri() ?>/images/promo-text.svg"
                 alt="Panama City Florida's Criminal Defense Attorney & Counselor">
        </div>
        <div class="promo-cta">
            <div class="cta-text">
                My office specializes primarily in criminal defense; however I also handle other various legal matters, such as wills, estate planning, and probate, and some personal injury matters.
            </div>
            <div class="cta-button"><a class="button outline button-block button-lg" href="<?php echo get_site_url(); ?>/contact-us">Contact Us</a></div>
        </div>
    </div>
</div>
<div class="container column-section">
    <a href="<?php echo get_site_url(); ?>/firm-overview/the-right-strategy" class="col-one-third block-link">
        <h3>The Right Strategy <i class="fa fa-angle-right" aria-hidden="true"></i></h3>

        <p>I advocate for people in all different types of circumstances, and each situation is unique. Maybe the allegations are false, maybe the officers violated your rights, or maybe you have other issues that need to be addressed.</p>

    </a>
    <a href="<?php echo get_site_url(); ?>/firm-overview/criminal-trial-representation" class="col-one-third block-link">
        <h3>Knowing The Landscape <i class="fa fa-angle-right" aria-hidden="true"></i></h3>

        <p>My practice is focused on representing all sorts of people at every level of the criminal justice system. I have over ten years of criminal trial experience in courtrooms all across the Florida Panhandle, primarily in Panama City.</p>
    </a>
    <a href="<?php echo get_site_url(); ?>/firm-overview/criminal-trial-attorney" class="col-one-third-last block-link">
        <h3>Criminal Trial Attorney <i class="fa fa-angle-right" aria-hidden="true"></i></h3>

        <p>If you have been arrested, or are under investigation for a criminal charge in Panama City, you need an experienced local attorney who can provide quality legal counsel to represent you and protect your interests.</p>
    </a>
</div>
<div class="container">
    <div class="quote">
        Having handled matters from civil traffic infractions all the way to life felonies and federal drug trafficking indictments and most everything in between, I am equally adept at negotiating a successful resolution, arguing legal technicalities in a motion, or presenting your case to a jury.
    </div>
</div>
<?php get_footer(); ?>
