<?php 
/*
Template Name: Sidebar Template
*/
?>
<?php get_header(); ?>

<div class="two-column-container">

  <?php if (have_posts()) : while (have_posts()) : the_post(); 
    include('includes/page-header.php') ?>

  <div class="column-two-thirds">
                <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>

    <article>
      
      <header>
        <h1><?php the_title(); ?></h1>    
      </header>

  		<?php the_content(); ?>

  	</article>
  </div><!--/.column-two-thirds-->

	<?php endwhile; endif; ?>

  <?php get_sidebar()?>

</div><!--/.two-column-container-->

<?php get_footer(); ?>

