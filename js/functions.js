/* 

@codekit-prepend "includes/html5shiv.js"
@codekit-append "includes/jquery.infinitescroll.min.js"
@codekit-append "includes/jquery.flexslider.js"

*/

// remap jQuery to $

(function($){

  $(window).load(function() {

  });

  $(document).ready(function (){

    //Toggle Menu

    function toggle_menu(){
      $('.menu-toggle, .responsive-menu').toggleClass('active');
      $('.site-wrapper,.top-nav').toggleClass('pushed');
    }

    $('.menu-toggle').click( function(){
      toggle_menu();
    });
/*  


    //Hide menu if scrolled too far
    $(window).scroll(function () {
      if ( $('#site-wrapper').hasClass('pushed')) {
        
        var menu_height = $('#main-menu ul').height();
        var distance = menu_height / 2
        
        if ($(window).scrollTop() > distance) {
          toggle_menu();
        }
      }
    });

    //Scroll to top
    $("a[href='#top']").click(function() {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
    });
*/

  });
})(jQuery);