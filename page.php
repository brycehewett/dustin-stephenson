<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();
  // include('includes/page-header.php') ?>

    <article>
      <header class="page-header">

        <div class="container">
          <h1><?php the_title(); ?></h1>
          <hr />
          <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>

          <?php
            $page_quote = get_post_meta( $post->ID, 'page_quote', true );
            // Check if the custom field has a value.
            if ( ! empty( $page_quote ) ) {
                echo "<div class='page-quote'>";
                echo $page_quote;
                echo "</div>";
            }?>
        </div>

      </header>

      <div class="single-column-container">
        <div class="container">
          <?php the_content(); ?>
        </div>
      </div>

  	</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>

