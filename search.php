<?php get_header(); ?>

<div class="two-column-container">
	<div class="column-two-thirds">

		<?php if (have_posts()) : ?>

			<h1>Search Results</h1>

		<?php while (have_posts()) : the_post();
			
			include('includes/post-teaser.php');
		
			endwhile;
		
			include (TEMPLATEPATH . '/includes/post-nav.php' );

			else : ?>
		
			<h2>No posts found.</h2>
	
		<?php endif; ?>
	  
	</div><!--/.column-two-thirds-->
	
	<?php get_sidebar(); ?>

</div><!--/.two-column-container-->

<?php get_footer(); ?>
